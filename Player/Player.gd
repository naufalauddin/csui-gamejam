extends KinematicBody2D

onready var camera = get_parent().get_node("Camera2D")
onready var attack_area = $AttackArea
onready var remote_transform = $RemoteTransform2D
onready var arm = $Arm
var stats = Stats

export(int) var MAX_SPEED = 900
export(int) var ACCELERATION = 2400
export var input_clamp = 1

var velocity = Vector2.ZERO

func _ready():
	stats.connect("no_health", self, "on_no_health")
	stats.emit_player(self)
	arm.visible = false
	stats.emit_player(self)

func _physics_process(delta):
	if Input.is_action_just_pressed("attack") and is_guard_reachable():
		var guard = attack_area.get_overlapping_bodies()[0]
		guard.possess()
		queue_free()

	movement(delta)
	suck(delta)

func movement(delta):
	var cursor_position = get_viewport().get_mouse_position()
	var viewport_size = get_viewport().size

	cursor_position = cursor_position - viewport_size/2
	cursor_position += camera.get_camera_screen_center()

	var input = global_position.direction_to(cursor_position)

	if input.length() < 0.1:
		velocity = velocity.move_toward(Vector2.ZERO, 2 * ACCELERATION * delta)

	elif input.length() < 10:
		input = input.clamped(input_clamp)
		velocity = velocity.move_toward(input * MAX_SPEED, 2 * ACCELERATION * delta)

	else:
		input = input.clamped(input_clamp)
		velocity = velocity.move_toward(input * MAX_SPEED, ACCELERATION * delta)

	velocity = move_and_slide(velocity)

func is_guard_reachable():
	var areas = attack_area.get_overlapping_bodies()
	return areas.size() > 0

func suck(delta):
	if is_guard_reachable():
		var guard = attack_area.get_overlapping_bodies()[0]
		arm.visible = true
		arm.look_at(guard.global_position)
		arm.get_node("TextureRect").rect_size.x = global_position.distance_to(guard.global_position)
		stats.health += 5 * delta
	else:
		stats.health -= 15 * delta
		arm.visible = false


func on_no_health():
	queue_free()

func _on_HurtBox_area_entered(area):
	stats.health -= 30
