extends Node2D

onready var player = get_node("Player")
onready var death_scene = $"CanvasLayer/Death Screen"
onready var stats = Stats

func _ready():
	stats.connect("player_change_node", self, "attach_remote_transform")
	stats.connect("no_health", self, "death_screen")
	stats.health = 100

func attach_remote_transform(player):
	var remote_transform = RemoteTransform2D.new()
	remote_transform.remote_path = NodePath("../../Camera2D")
	player.add_child(remote_transform)

func death_screen():
	death_scene.visible = true
