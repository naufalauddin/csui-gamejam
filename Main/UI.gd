extends Control

var health = 100 setget set_health

onready var health_ui = $Health

func set_health(amount):
	health = amount
	health_ui.rect_size.x = amount * 10

func _ready():
	self.health = Stats.health
	Stats.connect("health_changed", self, "set_health")
