extends LinkButton

export (String) var scene_to_load

func _on_LinkButton_pressed():
	get_tree().change_scene("res://Main/"+scene_to_load+".tscn")
