extends Node

export var MAX_HEALTH = 100
var health = MAX_HEALTH setget set_health
onready var player = get_tree().current_scene.get_node("Player")

signal no_health
signal health_changed(value)
signal player_change_node(body)

func set_health(amount):
	health = clamp(amount, 0, MAX_HEALTH)
	emit_signal("health_changed", health)
	if health <= 0:
		emit_signal("no_health")

func emit_player(body):
	player = body
	emit_signal("player_change_node", body)

func restore_health():
	health = MAX_HEALTH
