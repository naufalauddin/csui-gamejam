extends Camera2D

onready var bottom_right = $Node/BottomRight
onready var up_left = $Node/UpLeft

func _ready():
	limit_left = up_left.position.x
	limit_top = up_left.position.y
	limit_bottom = bottom_right.position.y
	limit_right = bottom_right.position.x
