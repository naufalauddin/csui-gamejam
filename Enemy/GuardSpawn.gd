extends Node2D

export var LOWEST_SPAWN_TIME = 10
export var HIGHEST_SPAWN_TIME = 20

onready var timer = $Timer
onready var Guard = preload("res://Enemy/Guard.tscn")

func _ready():
	randomize()
	start_timer_rand()

func _on_Timer_timeout():
	var guard = Guard.instance()
	var main = get_tree().current_scene
	main.add_child(guard)
	guard.global_position = global_position
	start_timer_rand()

func start_timer_rand():
	timer.start(rand_range(LOWEST_SPAWN_TIME, HIGHEST_SPAWN_TIME))
