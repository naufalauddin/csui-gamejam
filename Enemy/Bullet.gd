extends KinematicBody2D

export var SPEED = 10

onready var hit = $Hit

var velocity = Vector2.ZERO

func _physics_process(delta):
	move_and_collide(velocity)

func set_velocity():
	velocity = Vector2(SPEED * cos(rotation), SPEED * sin(rotation))

func own_by_player():
	hit.collision_layer = 32
	hit.collision_mask = 4

func _on_Hit_area_entered(area):
	queue_free()
