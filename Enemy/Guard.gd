extends KinematicBody2D

const Bullet = preload("res://Enemy/Bullet.tscn")

export var MAX_SPEED = 200
export var ACCELERATION = 600
export var DECELERATION = 500
export var MAX_HEALTH = 500
export var PUSH_SPEED = 300
export var POSSESSION_TIME = 5
export var reload_time = 2

onready var player = get_parent().get_node("Player")
onready var Player = preload("res://Player/Player.tscn")
onready var reload_timer = $ReloadTimer
onready var possessed_timer = $PossessedTimer
onready var soft_collision = $SoftCollision
onready var fire_detection = $FireDetection
onready var collision_shape = $CollisionShape2D
onready var sprite = $Sprite
onready var hurt_box = $HurtBox
onready var stats = Stats

var velocity = Vector2.ZERO
var state = CHASE
var health = MAX_HEALTH
var has_possessed = false

enum {
	AIM,
	CHASE,
	FIRE,
	POSSESSED,
}

func _ready():
	stats.connect("player_change_node", self, "set_new_player")
	player = stats.player

func _physics_process(delta):
	match state:
		CHASE:
			if player != null:
				accelerate_toward_point(player.global_position, delta)
				look_at_player()
			else:
				velocity = velocity.move_toward(Vector2.ZERO, DECELERATION * delta)
		AIM:
			if player != null:
				velocity = velocity.move_toward(Vector2.ZERO, DECELERATION * delta)
				look_at_player()
		POSSESSED:
			velocity = velocity.move_toward(Vector2.ZERO, DECELERATION * delta)
			if enemy_reachable():
				look_at_guards()
			else:
				start_random_look()

	if soft_collision.is_colliding():
		velocity += soft_collision.get_push_vector() * delta * PUSH_SPEED

	if enemy_reachable() && state != POSSESSED:
		state = AIM

	velocity = move_and_slide(velocity)

func accelerate_toward_point(point, delta):
	var direction = global_position.direction_to(point)
	velocity = velocity.move_toward(direction * MAX_SPEED, ACCELERATION * delta)

func _on_FireDetection_body_entered(body):
	if state != POSSESSED:
		state = AIM
	reload_timer.start(reload_time)

func look_at_player():
	look_at(player.global_position)

func _on_FireDetection_body_exited(body):
	if state != POSSESSED:
		state = CHASE
		reload_timer.stop()

func fire():
	var bullet = Bullet.instance()
	var main = get_tree().current_scene
	main.add_child(bullet)
	bullet.global_position = global_position
	bullet.rotation = rotation
	if state == POSSESSED:
		bullet.own_by_player()
	bullet.set_velocity()

func look_at_guards():
	var guards = fire_detection.get_overlapping_bodies()
	if guards.size() > 0:
		var guard = guards[0]
		look_at(guard.global_position)

func possess():
	state = POSSESSED
	fire_detection.get_node("CollisionShape2D").disabled = true
	collision_shape.disabled = true
	fire_detection.collision_mask = 4
	collision_layer = 2
	hurt_box.collision_mask = 64
	fire_detection.get_node("CollisionShape2D").disabled = false
	collision_shape.disabled = false
	reload_time = 0.5
	sprite.frame = 2
	possessed_timer.start(POSSESSION_TIME)
	stats.restore_health()
	stats.emit_player(self)

func set_new_player(body):
	player = body

func enemy_reachable():
	var enemies = fire_detection.get_overlapping_bodies()
	return enemies.size() > 0

func start_random_look():
	pass

func _on_Timer_timeout():
	fire()
	if enemy_reachable():
		reload_timer.start(reload_time)

func _on_HurtBox_area_entered(area):
	if state != POSSESSED:
		dead()

func dead():
	queue_free()

func _on_PossessedTimer_timeout():
	var player = Player.instance()
	var main = get_tree().current_scene
	main.add_child(player)
	player.global_position = global_position
	stats.emit_player(player)
	dead()
